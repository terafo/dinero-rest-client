object dmMain: TdmMain
  OldCreateOrder = False
  Height = 420
  Width = 617
  object RESTClient1: TRESTClient
    Accept = 'application/json, text/plain; q=0.9, text/html;q=0.8,'
    AcceptCharset = 'UTF-8, *;q=0.8'
    BaseURL = 'https://authz.dinero.dk/dineroapi/oauth/token'
    ContentType = 'application/x-www-form-urlencoded'
    Params = <>
    HandleRedirects = True
    RaiseExceptionOn500 = False
    Left = 48
    Top = 24
  end
  object RESTRequest1: TRESTRequest
    Client = RESTClient1
    Method = rmPOST
    Params = <
      item
        Kind = pkHTTPHEADER
        name = 'Authorization'
        Value = 
          'Basic UEZUZXJhOkQ2ZXBHQ2Rralg0WnNtRjdIQU4wR3pFc3VleWVoM3Jwa0FVZU' +
          'Z1Sjg0'
      end
      item
        Kind = pkHTTPHEADER
        name = 'Content-Type'
        Value = 'application/x-www-form-urlencoded'
        ContentType = ctAPPLICATION_X_WWW_FORM_URLENCODED
      end
      item
        Kind = pkREQUESTBODY
        name = 'grant_type'
        Value = 
          'password&scope=read write&username=ef03de271b7d452e97fb96787dabb' +
          'd56& password=ef03de271b7d452e97fb96787dabbd56'
      end>
    Response = RESTResponse1
    SynchronizedEvents = False
    Left = 128
    Top = 24
  end
  object RESTResponse1: TRESTResponse
    ContentType = 'application/json'
    Left = 208
    Top = 24
  end
end
